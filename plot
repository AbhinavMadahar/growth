#!/usr/bin/env python

from sys import argv

from matplotlib import pyplot as plt
from scipy.signal import medfilt

data = []

with open(argv[1], 'r') as trend_file:
    for line in trend_file:
        if '2018-08:' in line:
            break

        if ':' in line:
            try:
                data.append(int(line.split(' ')[1]))
            except ValueError:
                pass

plt.title('%s Popularity From 2013-2018'%argv[2])
plt.xticks(range(0, 120, 12))
plt.xlabel('Months since January 2013')
plt.ylabel('Frequency of Term Across the arXiv')
plt.plot(medfilt(data))
plt.savefig(argv[1].split('.')[0] + '.png')
